using System;
using System.Threading;
using CodeLock.Core.CodeLockStates;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core
{
    public class CodeLock : ICodeLock
    {
        public bool IsLockOpened { get; set; }
        public Timer LockTimer;

        private readonly ILockStatusNotifier _lockStatusNotifier;
        private readonly ISignalNotifier _signalNotifier;
        private readonly ICodeLockStateManager _stateManager;
        private readonly object _lockObj = new object();
        private ICodeLockState _state;


        public CodeLock(ILockStatusNotifier lockStatusNotifier,
            ICodeLockStateManager stateManager,
            ISignalNotifier signalNotifier)
        {
            _lockStatusNotifier = lockStatusNotifier;
            _stateManager = stateManager;
            _signalNotifier = signalNotifier;
            _state = _stateManager.GetState<ClosedState>(this);
        }

        public void TransitionTo(ICodeLockState state)
        {
            _state = state;
        }

        public void TakeDigitCode(string code)
        {
            if (code.Length != 4)
                throw new NotSupportedException();

            lock (_lockObj)
            {
                _state.CheckCode(code);
            }
        }

        public void CallButtonHandler()
        {
            lock (_lockObj)
            {
                _state.SendSignal();
            }
        }

        public void CallButonHoldHandler()
        {
            lock (_lockObj)
            {
                _state.SendModifier();
            }
        }

        public void ControlButtonHandler()
        {
            lock (_lockObj)
            {
                _state.ControlHandler();
            }
        }

        public void OpenLock()
        {
            IsLockOpened = true;
            SetAutoLockTimer();
            _lockStatusNotifier.LockOpened();
        }

        public void SendSignal()
        {
            _signalNotifier.Notify();
        }

        public void ResetAutoLockTimer()
        {
            LockTimer?.Dispose();
        }

        public void SetAutoLockTimer()
        {
            LockTimer?.Dispose();
            LockTimer = new Timer(_ =>
            {
                IsLockOpened = false;
                TransitionTo(_stateManager.GetState<ClosedState>(this));
                _lockStatusNotifier.LockClosed();
                LockTimer.Dispose();
            }, null, 5000, Timeout.Infinite);
        }
    }
}