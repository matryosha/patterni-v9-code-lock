using System.Collections.Generic;
using System.Linq;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core
{
    public class CodeLockStateManager : ICodeLockStateManager
    {
        private readonly ICodeLockStatesFactory _codeLockStatesFactory;
        private List<KeyValuePair<CodeLock,ICodeLockState>> _states = new List<KeyValuePair<CodeLock, ICodeLockState>>();

        public CodeLockStateManager(ICodeLockStatesFactory codeLockStatesFactory)
        {
            _codeLockStatesFactory = codeLockStatesFactory;
        }

        public ICodeLockState GetState<T>(CodeLock context) where T :class, ICodeLockState
        {
            var selectedState = _states.FirstOrDefault(s => s.Key == context && s.Value.GetType() == typeof(T)).Value;
            if (selectedState != null) return selectedState;

            var newState = _codeLockStatesFactory.CreateLockState<T>();
            newState.SetContext(context);
            _states.Add(new KeyValuePair<CodeLock, ICodeLockState>(context, newState));
            return newState;
        }
    }
}