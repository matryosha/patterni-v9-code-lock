using System.Collections.Generic;
using CodeLock.Core.CodeLockStates.Base;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core.CodeLockStates
{
    public class ChangingSecurityCodeState : BaseState, ICodeLockState
    {
        private readonly ICodeLockStateManager _codeLockStateManager;
        private readonly IPasswordManager _passwordManager;

        public ChangingSecurityCodeState(
            ICodeLockStateManager codeLockStateManager,
            IPasswordManager passwordManager)
        {
            _codeLockStateManager = codeLockStateManager;
            _passwordManager = passwordManager;
        }
        public new bool CheckCode(string digitsCode)
        {
            _passwordManager.SetSecurityCode(digitsCode);
            _context.SetAutoLockTimer();
            _context.TransitionTo(
                _codeLockStateManager.GetState<OpenedState>(_context));
            return true;
        }
    }
}