using CodeLock.Core.CodeLockStates.Base;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core.CodeLockStates
{
    public class ClosedState : BaseState, ICodeLockState
    {
        private readonly ICodeLockStateManager _codeLockStateManager;
        private readonly IPasswordManager _passwordManager;

        public ClosedState(ICodeLockStateManager codeLockStateManager,
            IPasswordManager passwordManager)
        {
            _codeLockStateManager = codeLockStateManager;
            _passwordManager = passwordManager;
        }

        public new bool CheckCode(string digitsCode)
        {
            if (digitsCode != _passwordManager.GetSecurityCode()) return false;
            _context.OpenLock();
            _context.TransitionTo(
                _codeLockStateManager.GetState<OpenedState>(_context));
            return true;
        }
    }
}