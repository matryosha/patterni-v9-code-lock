using CodeLock.Core.Interfaces;

namespace CodeLock.Core
{
    public class DefaultPasswordManager : IPasswordManager
    {
        private string _securityCode = "1111";
        private string _controlCode = "1111";
        
        public string GetSecurityCode() => _securityCode;

        public string GetControlCode() => _controlCode;

        public void SetSecurityCode(string code)
        {
            _securityCode = code;
        }

        public void SetControlCode(string code)
        {
            _controlCode = code;
        }
    }
}