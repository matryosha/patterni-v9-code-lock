namespace CodeLock.Core.Interfaces
{
    public interface ICodeLockStatesFactory
    {
        T CreateLockState<T>() where T : class, ICodeLockState;
    }
}