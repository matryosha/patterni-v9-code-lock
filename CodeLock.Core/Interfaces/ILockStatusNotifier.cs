namespace CodeLock.Core.Interfaces
{
    public interface ILockStatusNotifier
    {
        void LockOpened();
        void LockClosed();
    }
}