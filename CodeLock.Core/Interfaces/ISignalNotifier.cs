namespace CodeLock.Core.Interfaces
{
    public interface ISignalNotifier
    {
        void Notify();
    }
}