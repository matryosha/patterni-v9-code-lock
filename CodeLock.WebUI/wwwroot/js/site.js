﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.
"use strict";
let isLocked = true;
const lockStatusTextElement = document.getElementById("lock-status-text");
var connection = new signalR.HubConnectionBuilder().withUrl("/codeLockHub").build();

connection.on("LockClosed", _ => displayLockClosed());
connection.on("LockOpened", _ => displayLockOpened());
connection.on("SignalCalled", _ => signalCalled());
connection.start();


(() => {
    const controlButton = document.getElementById("control-btn");
    controlButton.addEventListener("click", () =>{
        connection.invoke("ControlButton");
    });
})();