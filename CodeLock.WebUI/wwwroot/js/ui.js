const display = document.getElementById("display");
const signalIcon = document.getElementById("signal-icon");
display.addEventListener('select', function () {
    this.selectionStart = this.selectionEnd;
}, false);
let signalTimeout;

function displayLockOpened(){
    display.className = 'display-lock-opened';
}

function displayLockClosed() {
    display.className = 'display-lock-closed';
}

function signalCalled() {
    signalIcon.setAttribute('class', 'signal-icon-active');
    clearTimeout(signalTimeout);
    signalTimeout = setTimeout(() => {
        signalIcon.setAttribute('class', '');
    }, 1000);
}